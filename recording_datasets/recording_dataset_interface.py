import os
import sys
import time

import cv2
from PySide6.QtCore import (
    QCoreApplication,
    QMetaObject,
    QSize,
    Qt,
    QThread,
    Signal,
)
from PySide6.QtGui import (
    QImage,
    QPixmap,
)
from PySide6.QtWidgets import (
    QApplication,
    QFrame,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QMainWindow,
    QPushButton,
    QSizePolicy,
    QVBoxLayout,
    QWidget,
    QMessageBox,
)
import resource_rc


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(836, 622)
        MainWindow.setStyleSheet(
            "QWidget {\n"
            "	background-color: #141619;\n"
            '	font: 87 12pt "Arial Black";\n'
            "}\n"
            "\n"
            "QPushButton {\n"
            "	background-color: #222028;\n"
            "	color: #f48400;\n"
            "	border-radius: 10px;\n"
            "}\n"
            "\n"
            "QPushButton:hover {\n"
            "	background-color: #f48400;\n"
            "	color: #fff;\n"
            "	border-radius: 10px;\n"
            "}\n"
            "\n"
            "QPushButton:pressed {\n"
            "	background-color: rgb(235, 125, 0);\n"
            "	color: #fff;\n"
            "	border-radius: 10px;\n"
            "}\n"
            "\n"
            "QLineEdit {\n"
            "	background-color: #222028;\n"
            "	color: #f48400;\n"
            '	font: 87 8pt "Arial Black";\n'
            "	border-radius: 10px;\n"
            "}\n"
            "\n"
            ""
        )
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.image_frame = QFrame(self.centralwidget)
        self.image_frame.setObjectName("image_frame")
        self.image_frame.setFrameShape(QFrame.StyledPanel)
        self.image_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.image_frame)
        self.horizontalLayout_3.setSpacing(10)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.video_frame = QFrame(self.image_frame)
        self.video_frame.setObjectName("video_frame")
        self.video_frame.setFrameShape(QFrame.StyledPanel)
        self.video_frame.setFrameShadow(QFrame.Raised)
        self.video_label = QLabel(self.video_frame)
        self.video_label.setGeometry(0, 10, 640, 480)

        self.horizontalLayout_3.addWidget(self.video_frame)

        self.abs_frame = QFrame(self.image_frame)
        self.abs_frame.setObjectName("abs_frame")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.abs_frame.sizePolicy().hasHeightForWidth())
        self.abs_frame.setSizePolicy(sizePolicy)
        self.abs_frame.setMaximumSize(QSize(300, 16777215))
        self.abs_frame.setStyleSheet("image: url(:/images/icons/abc.png);")
        self.abs_frame.setFrameShape(QFrame.StyledPanel)
        self.abs_frame.setFrameShadow(QFrame.Raised)

        self.horizontalLayout_3.addWidget(self.abs_frame)

        self.verticalLayout.addWidget(self.image_frame)

        self.main_frame = QFrame(self.centralwidget)
        self.main_frame.setObjectName("main_frame")
        self.main_frame.setMaximumSize(QSize(16777215, 100))
        self.main_frame.setFrameShape(QFrame.StyledPanel)
        self.main_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.main_frame)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.buttons_frame = QFrame(self.main_frame)
        self.buttons_frame.setObjectName("buttons_frame")
        self.buttons_frame.setMaximumSize(QSize(600, 16777215))
        self.buttons_frame.setFrameShape(QFrame.StyledPanel)
        self.buttons_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.buttons_frame)
        self.horizontalLayout_2.setSpacing(30)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.start_video = QPushButton(self.buttons_frame)
        self.start_video.setObjectName("start_video")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.start_video.sizePolicy().hasHeightForWidth())
        self.start_video.setSizePolicy(sizePolicy1)

        self.horizontalLayout_2.addWidget(self.start_video)

        self.save_images = QPushButton(self.buttons_frame)
        self.save_images.setObjectName("save_images")
        sizePolicy1.setHeightForWidth(self.save_images.sizePolicy().hasHeightForWidth())
        self.save_images.setSizePolicy(sizePolicy1)

        self.horizontalLayout_2.addWidget(self.save_images)

        self.horizontalLayout.addWidget(self.buttons_frame)

        self.combo_box_frame = QFrame(self.main_frame)
        self.combo_box_frame.setObjectName("combo_box_frame")
        self.combo_box_frame.setMaximumSize(QSize(200, 16777215))
        self.combo_box_frame.setFrameShape(QFrame.StyledPanel)
        self.combo_box_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.combo_box_frame)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.prefix = QLineEdit(self.combo_box_frame)
        self.prefix.setObjectName("lineEdit")
        self.prefix.setFrame(True)

        self.verticalLayout_2.addWidget(self.prefix)

        self.chosen_letter = QLineEdit(self.combo_box_frame)
        self.chosen_letter.setObjectName("chosen_letter")
        self.chosen_letter.setMaxLength(1)

        self.verticalLayout_2.addWidget(self.chosen_letter)

        self.horizontalLayout.addWidget(self.combo_box_frame)

        self.verticalLayout.addWidget(self.main_frame)

        MainWindow.setCentralWidget(self.centralwidget)
        self.camera_active = False
        self.dataset = []

        self.player = VideoPlayer()
        self.save_dataset = SaveDataset(self)
        self.player.start()
        self.save_dataset.start()
        self.player.ImageUpdate.connect(self.image_update_slot)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    # setupUi
    def image_update_slot(self, img: QImage):
        self.video_label.setPixmap(QPixmap.fromImage(img))
        if self.camera_active:
            self.dataset.append(img)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(
            QCoreApplication.translate("MainWindow", "MainWindow", None)
        )
        self.start_video.setText(
            QCoreApplication.translate(
                "MainWindow",
                "\u041d\u0430\u0447\u0430\u0442\u044c \u0437\u0430\u043f\u0438\u0441\u044c",
                None,
            )
        )
        self.save_images.setText(
            QCoreApplication.translate(
                "MainWindow",
                "\u0421\u043e\u0445\u0440\u0430\u043d\u0438\u0442\u044c \u043a\u0430\u0434\u0440\u044b",
                None,
            )
        )
        self.prefix.setInputMask("")
        self.prefix.setText("")
        self.prefix.setPlaceholderText(
            QCoreApplication.translate(
                "MainWindow", "\u041f\u0440\u0435\u0444\u0438\u043a\u0441", None
            )
        )
        self.chosen_letter.setInputMask("")
        self.chosen_letter.setText("")
        self.chosen_letter.setPlaceholderText(
            QCoreApplication.translate(
                "MainWindow", "\u0411\u0443\u043a\u0432\u0430", None
            )
        )

    # retranslateUi


class VideoPlayer(QThread):
    ImageUpdate = Signal(QImage)

    def run(self):
        self.thread_active = True
        cap = cv2.VideoCapture(0)
        while self.thread_active:
            ret, frame = cap.read()
            if ret:
                img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                flipped_img = cv2.flip(img, 1)
                convert_to_qt_format = QImage(
                    flipped_img.data,
                    flipped_img.shape[1],
                    flipped_img.shape[0],
                    QImage.Format_RGB888,
                )
                Pic = convert_to_qt_format.scaled(640, 480, Qt.KeepAspectRatio)
                self.ImageUpdate.emit(Pic)

    def stop(self):
        self.thread_active = False


class SaveDataset(QThread):
    def __init__(self, main_window: Ui_MainWindow) -> None:
        super().__init__()

        self.window = main_window

    def run(self):
        self.window.dataset = []
        self.window.camera_active = True
        time.sleep(1)
        self.window.camera_active = False

    def save_data(self):
        if self.window.prefix.text() == "" or self.window.chosen_letter.text() == "":
            error = QMessageBox()
            error.setWindowTitle("Ошибка")
            error.setText("Не указан префикс и/или буква")
            error.setIcon(QMessageBox.Warning)
            error.setStandardButtons(QMessageBox.Ok)
            error.exec()
        else:
            count = 1
            path = os.path.join(
                self.window.chosen_letter.text(), self.window.prefix.text()
            )
            for img in self.window.dataset:
                if not os.path.exists(path):
                    os.makedirs(path)
                img.save(os.path.join(path, f"{count}.jpg"), "JPG")
                count += 1
            self.window.dataset = []


class SupportApp(QMainWindow, Ui_MainWindow):
    def __init__(self) -> None:
        super().__init__()
        self.setupUi(self)

        self.start_video.clicked.connect(self.save_dataset.start)
        self.save_images.clicked.connect(self.save_dataset.save_data)

    def closeEvent(self, event) -> None:
        self.player.terminate()


if __name__ == "__main__":
    app = QApplication()
    window = SupportApp()
    window.show()
    sys.exit(app.exec())
