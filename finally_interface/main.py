import sys
from typing import Tuple


from PySide6 import QtCore
from PySide6 import QtGui
from PySide6 import QtWidgets


class Ui_MainWindow(object):
    def setup_ui(self, main_window: QtWidgets.QMainWindow) -> None:
        self.set_fonts()
        self.set_icons()
        self.set_styles()

        self.camera_menu_active = True
        self.file_menu_active = False
        self.settings_menu_active = False

        # Задаём параметры окна приложения
        main_window.setEnabled(True)
        main_window.resize(900, 600)
        main_window.setStyleSheet(self.main_window_style)
        # Создаём центральный виджет
        self.central_widget = QtWidgets.QWidget(main_window)

        # Layout для корректировки размеров Frame
        self.main_layout = QtWidgets.QVBoxLayout(self.central_widget)
        self.main_layout.setSpacing(0)
        self.main_layout.setContentsMargins(0, 0, 0, 0)

        # Самый верхний Frame, в котором находятся кнопки закрытия/сворачивания/уменьшения/увелечения приложения
        self.header_frame = QtWidgets.QFrame(self.central_widget)
        self.header_frame.setMaximumSize(QtCore.QSize(16777215, 30))
        self.header_frame.setMinimumSize(QtCore.QSize(0, 30))
        self.header_frame.setStyleSheet(self.header_frame_min)
        
        # Добавляем layout верхнего фрейма
        self.header_layout = QtWidgets.QHBoxLayout(self.header_frame)
        self.header_layout.setSpacing(0)
        self.header_layout.setContentsMargins(0, 0, 0, 0)

        # Добавляем Frame для логотипа команды
        self.logo_frame = QtWidgets.QFrame(self.header_frame)
        self.header_layout.addWidget(self.logo_frame)

        # Создаём Frame навигационных кнопок
        self.nav_frame = QtWidgets.QFrame(self.header_frame)
        self.nav_frame.setMaximumSize(QtCore.QSize(120, 16777215))

        # Создаём layout для навигационных кнопок
        self.nav_frame_layout = QtWidgets.QHBoxLayout(self.nav_frame)
        self.nav_frame_layout.setSpacing(0)
        self.nav_frame_layout.setContentsMargins(0, 0, 0, 0)

        # Создаём кнопку для сворачивания окна
        self.collapse_button = self.create_button(
            self.nav_frame,
            self.collapse_icon,
            (40, 30),
            (20, 20),
            self.nav_buttons_style
        )
        # Добавляем кнопку в layout
        self.nav_frame_layout.addWidget(self.collapse_button)

        # Создаём кнопку для изменения размеров окна
        self.resize_button = self.create_button(
            self.nav_frame,
            self.maximize_icon,
            (40, 30),
            (20, 20),
            self.nav_buttons_style
        )
        # Добавляем кнопку в layout
        self.nav_frame_layout.addWidget(self.resize_button)

        # Создаём кнопку выхода из приложения
        self.exit_button = self.create_button(
            self.nav_frame,
            self.exit_icon,
            (40, 30),
            (20, 20),
            self.nav_buttons_style,
        )

        # Добавляем кнопку в layout
        self.nav_frame_layout.addWidget(self.exit_button)

        # Добавляем Frame в layout верхнего Frame
        self.header_layout.addWidget(self.nav_frame)

        # Добавляем Frame header_frame в layout
        self.main_layout.addWidget(self.header_frame)
        
        # Создаём Frame основного окна
        self.main_frame = QtWidgets.QFrame(self.central_widget)

        # Создаём layout основного окна
        self.main_frame_layout = QtWidgets.QHBoxLayout(self.main_frame)
        self.main_frame_layout.setSpacing(0)
        self.main_frame_layout.setContentsMargins(0, 0, 0, 0)

        # Создаём frame бокового меню
        self.side_menu_frame = QtWidgets.QFrame(self.main_frame)
        self.side_menu_frame.setMaximumSize(QtCore.QSize(70, 16777215))
        self.side_menu_frame.setMinimumSize(QtCore.QSize(70, 0))
        
        # Создаём layout бокового меню
        self.side_menu_layout = QtWidgets.QVBoxLayout(self.side_menu_frame)
        self.side_menu_layout.setContentsMargins(5, 5, 5, 5)
        
        # Создаём кнопку активации меню
        self.cam_button = self.create_button(
            self.side_menu_frame,
            self.camera_icon,
            (70, 60),
            (30, 30),
            self.active_menu_buttons_style
        )
        # Добавляем кнопку в боковое меню
        self.side_menu_layout.addWidget(self.cam_button)

        # Создаём кнопку выбора файла
        self.choose_file_button = self.create_button(
            self.side_menu_frame,
            self.file_icon,
            (70, 60),
            (30, 30),
            self.inactive_menu_buttons_style,
        )
        # Добавляем кнопку в боковое меню
        self.side_menu_layout.addWidget(self.choose_file_button)

        # Создаём кнопку настроек
        self.settings_button = self.create_button(
            self.side_menu_frame,
            self.settings_icon,
            (70, 60),
            (30, 30),
            self.inactive_menu_buttons_style,
        )
        # Добавляем кнопку в боковое меню
        self.side_menu_layout.addWidget(self.settings_button)

        self.auxiliary_frame = QtWidgets.QFrame(self.side_menu_frame)
        self.side_menu_layout.addWidget(self.auxiliary_frame)

        self.main_frame_layout.addWidget(self.side_menu_frame)

        self.pages_layout = QtWidgets.QGridLayout()
        self.pages_layout.setSpacing(0)
        self.pages_layout.setContentsMargins(0, 0, 5, 0)

        self.pages = QtWidgets.QStackedWidget(self.main_frame)
        self.pages.setStyleSheet(self.pages_style)

        self.camera_page = QtWidgets.QWidget()

        self.camera_page_layout = QtWidgets.QGridLayout(self.camera_page)
        self.camera_page_layout.setSpacing(0)
        self.camera_page_layout.setContentsMargins(0, 0, 0, 0)
        
        self.camera_page_label = QtWidgets.QLabel(self.camera_page)
        self.camera_page_label.setFont(self.font)
        self.camera_page_label.setAlignment(QtCore.Qt.AlignCenter)
        self.camera_page_label.setText("Страница видео")

        self.camera_page_layout.addWidget(self.camera_page_label, 0, 0, 1, 1)

        self.pages.addWidget(self.camera_page)

        self.file_page = QtWidgets.QWidget()

        self.file_page_layout = QtWidgets.QGridLayout(self.file_page)
        self.file_page_layout.setSpacing(0)
        self.file_page_layout.setContentsMargins(0, 0, 0, 0)

        self.file_page_label = QtWidgets.QLabel(self.file_page)
        self.file_page_label.setFont(self.font)
        self.file_page_label.setAlignment(QtCore.Qt.AlignCenter)
        self.file_page_label.setText("Выбор файла")

        self.file_page_layout.addWidget(self.file_page_label, 0, 0, 1, 1)

        self.pages.addWidget(self.file_page)

        self.settings_page = QtWidgets.QWidget()

        self.settings_page_layout = QtWidgets.QGridLayout(self.settings_page)
        self.settings_page_layout.setSpacing(0)
        self.settings_page_layout.setContentsMargins(0, 0, 0, 0)

        self.settings_page_label = QtWidgets.QLabel(self.settings_page)
        self.settings_page_label.setFont(self.font)
        self.settings_page_label.setAlignment(QtCore.Qt.AlignCenter)
        self.settings_page_label.setText("Меню настроек")

        self.settings_page_layout.addWidget(self.settings_page_label, 0, 0, 1, 1)

        self.pages.addWidget(self.settings_page)

        self.pages_layout.addWidget(self.pages, 0, 0, 1, 1)

        self.main_frame_layout.addLayout(self.pages_layout)

        self.main_layout.addWidget(self.main_frame)

        self.bottom_frame = QtWidgets.QFrame(self.central_widget)
        self.bottom_frame.setMaximumSize(QtCore.QSize(16777215, 30))
        self.bottom_frame.setStyleSheet(self.bottom_frame_min)

        self.bottom_layout = QtWidgets.QHBoxLayout(self.bottom_frame)

        self.version_label = QtWidgets.QLabel(self.bottom_frame)
        self.version_label.setText("FinABC @V1")

        self.bottom_layout.addWidget(self.version_label)

        self.main_layout.addWidget(self.bottom_frame)

        main_window.setCentralWidget(self.central_widget)

        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

    def add_icon(self, icon: QtGui.QIcon, path: str):
        icon.addFile(
            path,
            QtCore.QSize(),
            QtGui.QIcon.Normal, 
            QtGui.QIcon.Off
        )
    
    def set_fonts(self):
        """Добавляем шрифты."""
        font_id = QtGui.QFontDatabase.addApplicationFont("other/Montserrat-Black.ttf")
        font_name = QtGui.QFontDatabase.applicationFontFamilies(font_id)
        self.font = QtGui.QFont(font_name, 18)

    def set_icons(self):
        """Установка всех иконок."""
        self.collapse_icon = QtGui.QIcon()
        self.add_icon(self.collapse_icon, u"icons/minus-sign.png")

        self.maximize_icon = QtGui.QIcon()
        self.add_icon(self.maximize_icon, u"icons/increase.png")

        self.minimize_icon = QtGui.QIcon()
        self.add_icon(self.minimize_icon, u"icons/reduce.png")

        self.exit_icon = QtGui.QIcon()
        self.add_icon(self.exit_icon, u"icons/reject.png")

        self.camera_icon = QtGui.QIcon()
        self.add_icon(self.camera_icon, u"icons/cam.png")

        self.file_icon = QtGui.QIcon()
        self.add_icon(self.file_icon, u"icons/file.png")

        self.settings_icon = QtGui.QIcon()
        self.add_icon(self.settings_icon, u"icons/settings.png")

    def set_styles(self):
        """Задание стилей приложения."""
        self.main_window_style = (
            u"QFrame {"
            u"background-color: #000;}"
            u"QPushButton {"
            u"background-color: rgb(0, 0, 0);"
            u"color: white;}"
            u"QPushButton:hover {"
            u"background-color: #f48400;"
            u"border-radius: 15px;}"
            u"QLabel {"
            u"background-color: #000;"
            u"color: #fff;"
            u"border-radius: 10px;}"
        )

        self.header_frame_max = (
            u"border-top-left-radius: 0px;"
            u"border-top-right-radius: 0px"
        )

        self.header_frame_min = (
            u"border-top-left-radius: 10px;"
            u"border-top-right-radius: 10px"
        )

        self.bottom_frame_max = (
            u"border-bottom-left-radius: 0px;"
            u"border-bottom-right-radius: 0px"
        )

        self.bottom_frame_min = (
            u"border-bottom-left-radius: 10px;"
            u"border-bottom-right-radius: 10px"
        )

        self.nav_buttons_style = (
            u"QPushButton {"
            u"background-color: rgb(0, 0, 0);"
            u"color: white;}"
            u"QPushButton:hover {"
            u"background-color: #f48400;"
            u"border-radius: 10px;}"
        )

        self.active_menu_buttons_style = (
            u"background-color: #f48400;"
            u"border-radius: 10px;"
        )
        
        self.inactive_menu_buttons_style = (
            u"QPushButton {"
            u"background-color: rgb(0, 0, 0);"
            u"color: white;}"
            u"QPushButton:hover {"
            u"background-color: #f48400;"
            u"border-radius: 15px;}"
        )

        self.pages_style = (
            u"QWidget {"
            u"background-color: rgb(67, 67, 67);"
            u"border-radius: 10px;}"
        )

    def create_button(
        self,
        frame: QtWidgets.QFrame,
        icon: QtGui.QIcon,
        button_size: Tuple[int, int],
        icon_size: Tuple[int, int],
        style_sheet: str,
    ):
        """Конструктор кнопок."""
        button = QtWidgets.QPushButton(frame)
        but_width, but_height = button_size
        i_width, i_height = icon_size
        button.setMaximumSize(QtCore.QSize(but_width, but_height))
        button.setStyleSheet(style_sheet)
        button.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        button.setIcon(icon)
        button.setIconSize(QtCore.QSize(i_width, i_height))
        return button


class App(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()

        self.setup_ui(self)
        
        self.window_size = 0

        self.exit_button.clicked.connect(self.close)
        self.resize_button.clicked.connect(self.resize_app)
        self.collapse_button.clicked.connect(self.showMinimized)

        self.cam_button.clicked.connect(self.camera_menu)
        self.choose_file_button.clicked.connect(self.file_menu)
        self.settings_button.clicked.connect(self.settings_menu)
        QtWidgets.QSizeGrip(self.bottom_frame)

        self.header_frame.mouseMoveEvent = self.move_window

    def resize_app(self):
        if self.window_size == 0:
            self.window_size = 1
            self.header_frame.setStyleSheet(self.header_frame_max)
            self.bottom_frame.setStyleSheet(self.bottom_frame_max)
            self.resize_button.setIcon(self.minimize_icon)
            self.resize_button.setIconSize(QtCore.QSize(20, 20))
            self.showFullScreen()
        else:
            self.window_size = 0
            self.header_frame.setStyleSheet(self.header_frame_min)
            self.bottom_frame.setStyleSheet(self.header_frame_min)
            self.resize_button.setIcon(self.maximize_icon)
            self.resize_button.setIconSize(QtCore.QSize(20, 20))
            self.showNormal()

    def move_window(self, event: QtGui.QMouseEvent) -> None:
        if self.isMaximized() == False:
            if event.buttons() == QtCore.Qt.LeftButton:
                self.move(self.pos() + event.globalPosition().toPoint() - self.dragPos)
                self.dragPos = event.globalPosition().toPoint()
                event.accept()

    def camera_menu(self):
        if not self.camera_menu_active:
            self.cam_button.setStyleSheet(self.active_menu_buttons_style)
            self.choose_file_button.setStyleSheet(self.inactive_menu_buttons_style)
            self.settings_button.setStyleSheet(self.inactive_menu_buttons_style)
            self.camera_menu_active = True
            self.file_menu_active = False
            self.settings_menu_active = False
            self.pages.setCurrentWidget(self.camera_page)

    def file_menu(self):
        if not self.file_menu_active:
            self.choose_file_button.setStyleSheet(self.active_menu_buttons_style)
            self.cam_button.setStyleSheet(self.inactive_menu_buttons_style)
            self.settings_button.setStyleSheet(self.inactive_menu_buttons_style)
            self.camera_menu_active = False
            self.file_menu_active = True
            self.settings_menu_active = False
            self.pages.setCurrentWidget(self.file_page)

    def settings_menu(self):
        if not self.settings_menu_active:
            self.settings_button.setStyleSheet(self.active_menu_buttons_style)
            self.cam_button.setStyleSheet(self.inactive_menu_buttons_style)
            self.choose_file_button.setStyleSheet(self.inactive_menu_buttons_style)
            self.camera_menu_active = False
            self.file_menu_active = False
            self.settings_menu_active = True
            self.pages.setCurrentWidget(self.settings_page)

    def mousePressEvent(self, event: QtGui.QMouseEvent) -> None:
        self.dragPos = event.globalPosition().toPoint()


if __name__ == "__main__":
    app = QtWidgets.QApplication()
    window = App()
    window.show()
    sys.exit(app.exec())
