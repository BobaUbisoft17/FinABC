import cv2
import mediapipe as mp
import numpy as np

from neural_networks_loader import detect
from utils import codecs


font                   = cv2.FONT_HERSHEY_COMPLEX
fontScale              = 2
fontColor              = (0,0,0)
thickness              = 3
lineType               = 2

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_hands = mp.solutions.hands

def read_video(path: str) -> None:
    # For static images:
    IMAGE_FILES = []
    with mp_hands.Hands(
        static_image_mode=True,
        max_num_hands=2,
        min_detection_confidence=0.5) as hands:
        for idx, file in enumerate(IMAGE_FILES):
            # Read an image, flip it around y-axis for correct handedness output (see
            # above).
            image = cv2.flip(cv2.imread(file), 1)
            # Convert the BGR image to RGB before processing.
            results = hands.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

            # Print handedness and draw hand landmarks on the image.
            print('Handedness:', results.multi_handedness)
            if not results.multi_hand_landmarks:
                continue
            image_height, image_width, _ = image.shape
            annotated_image = image.copy()
            for hand_landmarks in results.multi_hand_landmarks:
                print('hand_landmarks:', hand_landmarks)
                print(
                    f'Index finger tip coordinates: (',
                    f'{hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].x * image_width}, '
                    f'{hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].y * image_height})'
                )
                mp_drawing.draw_landmarks(
                    annotated_image,
                    hand_landmarks,
                    mp_hands.HAND_CONNECTIONS,
                    mp_drawing_styles.get_default_hand_landmarks_style(),
                    mp_drawing_styles.get_default_hand_connections_style())
            cv2.imwrite(
                '/tmp/annotated_image' + str(idx) + '.png', cv2.flip(annotated_image, 1))
            # Draw hand world landmarks.
            if not results.multi_hand_world_landmarks:
                continue
            for hand_world_landmarks in results.multi_hand_world_landmarks:
                mp_drawing.plot_landmarks(
                    hand_world_landmarks, mp_hands.HAND_CONNECTIONS, azimuth=5)

    # For input:
    if path == 0:
        name = "Camera"
    else:
        name = "file"
    if name == "Camera" or f".{path.split('.')[-1]}" in codecs[:-4]: # TODO: исправить на отдельный список видео и изображений
        cap = cv2.VideoCapture(path)
        with mp_hands.Hands(
            model_complexity=0,
            min_detection_confidence=0.5,
            min_tracking_confidence=0.5) as hands:
            while cap.isOpened():
                success, image = cap.read()
                if not success:
                    cv2.destroyAllWindows()
                    break
                draw_hand_image(hands, image, name)
                if cv2.waitKey(1) & 0xFF == 27:
                    cv2.destroyAllWindows()
                    break
        cap.release()
    else:
        image = cv2.imdecode(np.fromfile(path, dtype=np.uint8), cv2.IMREAD_UNCHANGED)
        with mp_hands.Hands(
            model_complexity=0,
            min_detection_confidence=0.5,
            min_tracking_confidence=0.5) as hands:
            draw_hand_image(hands, image, name)
            if cv2.waitKey(0) & 0xFF == 27:
                cv2.destroyAllWindows()


def draw_hand_image(hands: mp.solutions.hands, image: cv2.Mat, name: str) -> None:
    # To improve performance, optionally mark the image as not writeable to
    # pass by reference.
    image.flags.writeable = False
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    results = hands.process(image)

    # Draw the hand annotations on the image.
    image.flags.writeable = True
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    if results.multi_hand_landmarks:
        letter = detect(results)
        print_letter(letter)
        # lm = results.multi_hand_landmarks[0].HandLandmark[0]
        # h, w, c =image.shape    
        # cx, cy = int(lm.x * w), int(lm.y * h) 
        cv2.putText(image, letter, 
            (10, 100), # (cx, cy), 
            font, 
            fontScale,
            fontColor,
            thickness,
            lineType)

        for hand_landmarks in results.multi_hand_landmarks:
            mp_drawing.draw_landmarks(
                image,
                hand_landmarks,
                mp_hands.HAND_CONNECTIONS,
                mp_drawing_styles.get_default_hand_landmarks_style(),
                mp_drawing_styles.get_default_hand_connections_style())
            
    # Flip the image horizontally for a selfie-view display.
    cv2.imshow(name, image)

letters_per_second = 1
# Из расчёта 30 кадров в секунду
letters_count = 30 / letters_per_second

recognized_letters = []

def print_letter(letter):
    """Выводит самую частую букву за период времени"""
    recognized_letters.append(letter)

    if len(recognized_letters) == letters_count:

        print(max(set(recognized_letters), key=lambda x: recognized_letters.count(x)))
        recognized_letters.clear()