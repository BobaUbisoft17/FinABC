import sys
from os import path
from PyQt5.QtGui import QFontDatabase, QFont, QIcon
from PyQt5.QtWidgets import QApplication, QFileDialog, QMainWindow, QMessageBox, QPushButton

from parse_cam import activate_cam
from parse_video import parse_video
from utils import codecs

def application():
    app = QApplication(sys.argv)
    window = QMainWindow()

    parent_dir = path.dirname(path.abspath(__file__))
    
    font_id = QFontDatabase.addApplicationFont(path.join(parent_dir, 'other', 'Montserrat-Black.ttf'))
    font_name = QFontDatabase.applicationFontFamilies(font_id)[0]
    font = QFont(font_name, 15)

    app.setWindowIcon(QIcon(path.join(parent_dir, 'other', 'abc-black-1.ico')))
    window.setWindowTitle("FinABC")
    window.setGeometry(0, 0, 600, 500)
    window.setStyleSheet(
        "background-color: rgb(59, 59, 59);"
        "color: rgb(255, 255, 255);"
    )

    choose_file_button = QPushButton(window)
    choose_file_button.setGeometry(0, 0, 300, 500)
    choose_file_button.setFont(font)
    choose_file_button.setText("Выбрать файл")

    activate_cam_button = QPushButton(window)
    activate_cam_button.setGeometry(300, 0, 300, 500)
    activate_cam_button.setFont(font)
    activate_cam_button.setText("Включить камеру")

    choose_file_button.clicked.connect(choose_file)
    activate_cam_button.clicked.connect(activate_cam)

    window.show()
    sys.exit(app.exec_())


def choose_file() -> None:
    dialog = QFileDialog()
    # dialog.setNameFilters(codecs) # TODO:Поискать способы фильтровать 
    path = dialog.getOpenFileName()[0]

    if not path:
        return

    codec = f".{path.split('.')[-1]}"
    if codec not in codecs and codec != ".":
        error = QMessageBox()
        error.setWindowTitle("Ошибка")
        error.setText(
            "Невозможно открыть файл. "
            "Выберите видеофайл или изображение."
        )
        error.setIcon(QMessageBox.Warning)
        error.setStandardButtons(QMessageBox.Ok)

        error.exec_()
    elif codec != ".":
        parse_video(path)
        

if __name__ == "__main__":
    application()