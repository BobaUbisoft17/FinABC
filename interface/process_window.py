
from PyQt5 import QtCore, QtWidgets

class ProceessWindow(object):
    """Представляет окно отображения работы алгоритма"""
    progressValue = 0

    def setup_ui(self, Dialog: QtWidgets.QDialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(299, 152)
        Dialog.setMinimumSize(QtCore.QSize(299, 152))
        Dialog.setMaximumSize(QtCore.QSize(299, 152))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Видеозапись или изображение"))