import torch
from os import path

parent_dir = path.dirname(path.dirname(path.abspath(__file__)))
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
model = torch.load(path.join(parent_dir, 'LSTM.pt'), map_location=device).eval()

class RnnModel():
    def __init__(self, model,device):
        self.model = model.to(device)
        self.q = torch.tensor([]).to(device)
        self.device = device
        self.c = 0

    def __call__(self,x):
        x = x.to(self.device)
        if len(self.q) < 30:
            if len(self.q):
                self.q = torch.cat((self.q, x.view(1,-1)),0)
            else:
                self.q =  x.view(1,-1)
        else:
            self.q[self.c % 30] = x

        otv, self.h = self.model(self.q[self.c%30].unsqueeze(0))
        for i in range(0, len(self.q)):
            j = (i+self.c%len(self.q)) % len(self.q)
            otv, self.h = self.model(self.q[j].unsqueeze(0), self.h)
        self.c += 1
        return otv

    def clear(self):
        self.h = None

model = RnnModel(model,device)
def detect(results):
    with torch.no_grad():
        letters = letters = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
        keypoints = []
        for landmark in results.multi_hand_landmarks[0].landmark:
            keypoints += [landmark.x,landmark.y,landmark.z]
        points = torch.tensor(keypoints).cuda().view(-1,3)
        eges = ((0,1),(0,5),(0,17),(5,9),(9,13),(13,17),
                (1,2),(2,1),(3,4),
                (5,6),(6,7),(7,8),
                (9,10),(10,11),(11,12),
                (13,14),(14,15),(15,16),
                (17,18),(18,19),(19,20))
        angles = torch.tensor([])
        for a, b in eges:
            a, b = points[a], points[b]
            vec = a - b
            l = (vec**2).sum()**0.5
            if len(angles):
                angles = torch.cat((angles,vec/l),0)
            else:
                angles = vec/l
        angles = torch.cat((angles, points[0]),0)

        return letters[model(angles).argmax()]
